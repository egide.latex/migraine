Changelog
=========

* http://keepachangelog.com/fr/1.0.0/


## [Unreleased]
Création du projet (Cf. infra)

### Added
- Structure git flow
- fichiers d'accompagnement.


## [1.0.0] 2017-12-20
### Ajouts
- Configuration latexmk intégrée
- Modèle d'exemple complet
- Cahiers anonymes pour:
  - l'année
  - quelques mois
  - 6, 10, 15 migraines mensuelles


### Changements
- Tableau sans médicaments (trop de molécules possibles)
- Des lignes pour écrire les métadonnées personnelles
- Cahiers séparés pour le calendrier et pour le journal des crises


Historique du projet
====================

*Date de création*: 2017-05-01


Sans amélioration ses dernières années, malgré un traitement de
fond suivi, mon médecin décide de m'envoyer chez un spécialiste. Il
me demande faire un journal des migraines.

Les journaux que je trouve, papier ou informatisé, ne me conviennent
pas. J'aimerais pouvoir ajouter la journalisation des habitudes de
sommeil et d'alimentation.


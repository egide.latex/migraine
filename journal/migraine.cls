\NeedsTeXFormat{LaTeX2e}%<1
\ProvidesClass{migraine}[2017/07/30 Alpha, V0.0]
%\LoadClassWithOptions{scrartcl}
\LoadClass[%
	headings=small,% petite écriture pour les titres
	numbers=noenddot,% supprime le point à la fin de la numérotation des titres
	fontsize=10pt,%
	frenchb,%
]{scrbook}

\KOMAoptions{%
	twoside,%
}

%<1 Paquets usuels
\RequirePackage{fontspec}
\RequirePackage{xltxtra}
\setmainfont[Mapping=tex-text]{Linux Libertine O}
\RequirePackage{pifont}%              police de dessin
\RequirePackage{xspace}%              gestion correcte des espaces après les macros
\RequirePackage{lastpage}%            connaître le numéro de la dernière page
\RequirePackage[%                     Gestion des entêtes et pieds de pages
	footsepline=.1pt,%
	headsepline=.1pt,%
	plainfootsepline=true%
]{scrlayer-scrpage}%
\RequirePackage{multicol}%            Pour mettre le texte en colonnes
\RequirePackage[np]{numprint}%        Mise en forme des chiffres
\RequirePackage{lineno}%              Numérotation des lignes
\RequirePackage[%                     Gestion des marges
	includeall,%                       Compte les marges hors tout
	nomarginpar,%                      Sans notes marginales
	bindingoffset=10mm,%               Marge de reliure
	hmargin=7.5mm,%
	headsep=5mm,
	vmargin=7mm,%
	footskip=7mm,
	pdftex,%
]{geometry}
\RequirePackage{hyperref}%            Gestion des liens hypertexte du PDF
\RequirePackage{graphicx}%            Pour inclure des images
\RequirePackage{enumitem}%            Personnalisation des listes
\RequirePackage{multido}%             Traitement itératif
\RequirePackage{tikz}%                Production des tableau et graphique
\RequirePackage{translator}%          Traduction des mois et jours en français
\uselanguage{French}%
\languagepath{French}%
\usetikzlibrary{calc,calendar}%       Modules pour la gestion des dates
\RequirePackage{lscape}%              Permet de tourner le contenu d'une page en mode paysage
\RequirePackage{ifthen}%              Pour créer des boucles conditionnelles
\RequirePackage{hyperref}%            Gestion des liens hypertexte du PDF
\RequirePackage[frenchb]{babel}%      Gestion des langues


%<1 Variables et constantes
%<2 Constantes
% Année de début du calendrier
\newcounter{annee}
\setcounter{annee}{\year}
\newcommand*\annee[1]{\setcounter{annee}{#1}}

% Mois de début du calendrier
\newcounter{mois}
\setcounter{mois}{1}
\newcommand*\mois[1]{\setcounter{mois}{#1}}

% Nombre moyen de migraines par mois
\newcounter{parmois}
\setcounter{parmois}{6}
\newcommand*\parmois[1]{\setcounter{parmois}{#1}}


%<1 Décoration de page
\lehead[]{\@titre}
\rehead[]{}
\lohead[]{}
\refoot[]{De \themois{} / \theannee{} pour \theduree{} mois}
\lofoot[]{imprimé le \@imprime}
\ofoot[]{\thepage/\pageref{LastPage}}

% Entête et pied de pages légèrement plus petits
\addtokomafont{pagehead}{\small}
\addtokomafont{pagefoot}{\small}


%<1 Page de titre
% Adresse du propriétaire
\newcommand\@btl[2][6ex]{\makebox[#1][r]{\em\footnotesize #2}~\makebox[24em]{\dotfill}\par\bigskip}
\newcommand\@adresse{%
	\@btl{Adresse~:}%
	\@btl{}%
	\@btl{Lieu~:}%
	\@btl{Tel.~:}%
	\@btl{Courriel~:}%
	}\newcommand\adresse[1]{\renewcommand\@adresse{#1}}
\uppertitleback{\@adresse}
\lowertitleback{%
	{\footnotesize%
	Échelle de douleur utilisée (probablement pas très conventionnelle).%
	\begin{multicols}{2}%
		\begin{description}%
			\item[1)] Douleur présente et bien installée;%
			\item[3)] Douleur handicapante pour l'activité en cours;%
			\item[5)] L'activité courante n'est plus possible (incapacité totale de travail);%
			\item[7)] Il n'y a plus rien d'autre que des pleurs;%
			\item[9)] La question se pose de savoir s'il faut faire un traitement pas le plomb;%
			\item[10)] Cette question ne se pose meme plus.%
		\end{description}%
	\end{multicols}%
	}%
	\tableofcontents%
}

% Prénom et nom
\newcommand*\@prenom{{\em\footnotesize Prénom~:}\makebox[7em]{\dotfill}\xspace}
\newcommand*\prenom[1]{\renewcommand*\@prenom{#1\xspace}}
\newcommand*\@nom{{\em\footnotesize Nom~:}\makebox[7em]{\dotfill}\xspace}
\newcommand*\nom[1]{\renewcommand*\@nom{#1\xspace}}
\subject{\@prenom \bsc{\@nom}}

% Titre
\newcommand*\@titre{Journal des céphalées\xspace}
\newcommand*\titre[1]{\renewcommand*\@titre{#1\xspace}}
\title{\@titre}

\newcommand*\@soustitre{\rule{0pt}{1em}\xspace}
\newcommand*\soustitre[1]{\renewcommand*\@soustitre{#1\xspace}}
\subtitle{\@soustitre}

% Médecin
\newcommand*\@medecin{{\em\footnotesize Médecin~:}\makebox[20em]{\dotfill}\xspace}
\newcommand*\medecin[1]{\renewcommand*\@medecin{#1}}
\author{\@medecin}

\titlehead{\begin{center}\LARGE\vspace{2em}\bsc{C o n f i d e n t i e l}\end{center}}
\publishers{Du 1\ier / \themois / \theannee{} pour \theduree{} mois}

% Imprimé le
\newcommand*\@imprime{\today\xspace}\newcommand*\imprime[1]{\renewcommand*\@imprime{#1\xspace}}
\date{\@imprime}

\newcommand\pagetitre{
	\hypersetup{%
		backref=true,%
		pagebackref=true,%     ...les bibliographies
		hyperindex=true,%      ajoute des liens dans les index.
		colorlinks=false,%     colories les liens
		breaklinks=true,%      permet le retour à la ligne dans les liens trop longs
		urlcolor= blue,%       couleur des hyperliens
		linkcolor= blue,%      couleur des liens internes
		bookmarks=true,%       créé des signets pour Acrobat
		bookmarksopen=false,%  si les signets Acrobat sont créés,
		pdftitle={\@titre},%
		pdfauthor={\@prenom \@nom},%
		pdfsubject={De \themois{} / \theannee{} pour \theduree{} mois},%
		pdfcreator={Egide},%
		pdfproducer={Vim, LaTeX2e, git et mes dix doigts},%
		pdfkeywords={journal, migraine, céphalée},%
	}%
	\newgeometry{hmargin=3cm}%
	\maketitle[-1]%
	\clearpage%
	\restoregeometry%
	\clearpage%
}


%<1 Page de notes
% \pagenote permet de créer un page de notes manuscrite.
%   l'argument optionel spécifie le nombre de pages à créer.
% \noteslignes permet de changer le nombre de lignes par page.
\newcounter{noteslignes}% nombre de ligne par page
\setcounter{noteslignes}{82}% lignes par défaut
\newcommand*\noteslignes[1]{\setcounter{noteslignes}{#1}}
\newcommand\pagenote[1][1]{
	\setlength{\columnsep}{1cm}%
	\setlength{\columnseprule}{1pt}%
	\setlength{\parindent}{0pt}%
	\rohead[]{Notes personnelles}%
	\addcontentsline{toc}{part}{Notes personnelles}%
	\addtocounter{noteslignes}{-1}%
	\multido{\i=1+1}{#1}{%
		\clearpage%
		\begin{multicols}{2}%
			\multido{\i=1+1}{\thenoteslignes}{%
				\rule{0pt}{4ex}\dotfill\\%
			}%
				\rule{0pt}{4ex}\dotfill%
		\end{multicols}
	}%
}


%<1 Pages mensuelles
% créé des tableaux mensuel de suivit des constantes.
% \calendriermensuel introduit les pages mensuelles
% \duree permet de changer la durée par défaut
%<2 Conteurs
\newcounter{duree}% Durée du calendrier en nombre de mois
\setcounter{duree}{12}
\newcommand*\duree[1]{\setcounter{duree}{#1}}
\newcounter{jourcourant}% Jour courant
\newcounter{lignejour}\setcounter{lignejour}{27}%position de la ligne des dates (hauteur)
\newcounter{colminus}\setcounter{colminus}{-5}% Nombre de colonnes avant le tableau
\newcounter{ligne}% Boucle de création de libellés de ligne
\newcounter{moiscourant}
\newcounter{anneecourant}

%<2 Commandes
\newcommand*\nligne[1]{\stepcounter{ligne}% Libellés
	\draw (1,\thelignejour.5-\theligne) node [left,scale=.8]{#1};%
}
\newcommand*\lignesep{% Trait de séparation horizontal
	\draw (\thecolminus,\thelignejour-\theligne)%
	-- (\thejourcourant+1,\thelignejour-\theligne);%
}
\newcommand*\lignemain{\stepcounter{ligne}% Trait écriture manuelle
	\draw [dotted] (\thecolminus,\thelignejour-\theligne)%
	-- (1,\thelignejour-\theligne);%
}

\newcommand\calendriermensuel{%<2
	\setcounter{anneecourant}{\theannee}%
	\setcounter{moiscourant}{\themois}%
	\setlength{\parindent}{0pt}%
	\rohead[]{Calendriers mensuels}%
	\addcontentsline{toc}{part}{Calendriers mensuels}%
	\multido{\i=1+1}{\theduree}{% boucle sur le nombre de mois à produire
		\setcounter{jourcourant}{1}%
		\ifthenelse{\equal{\themoiscourant}{13}}{\stepcounter{anneecourant}\setcounter{moiscourant}{1}}{}%
		\rohead[]{Calendriers mensuels}%
		\clearpage%
		\begin{landscape}%
			\centering%
			\begin{tikzpicture}[xscale=.689,yscale=.657]% Tableau
				%<3 Jours
				\calendar[%
					dates=\theanneecourant-\themoiscourant-01 to \theanneecourant-\themoiscourant-last,%
					name=cal,%
					execute before day scope={%
						\ifdate{day of month=1}{% Affiche le nom du mois
							\draw (\thecolminus,\thelignejour-.3) node [right]%
								{\textbf{\theanneecourant{} \pgfcalendarmonthname{\pgfcalendarcurrentmonth}}};%
						}{}%
					},%
					day code={%
						\setcounter{jourcourant}{\pgfcalendarcurrentday}%
						\ifdate{weekend}% Colonnes grisées pour les samedis dimanches
							{\fill [color=gray!40] (\thejourcourant,0) rectangle (\thejourcourant+1,\thelignejour);%
						}{}%
						\draw (\thejourcourant+.5,\thelignejour-.3) node [scale=.6] {\tikzdaytext};%
						\draw (\thejourcourant+.5,\thelignejour-.7) node%
							{\tiny\pgfcalendarweekdayshortname{\pgfcalendarcurrentweekday}};%
					},%
				];%
				%<3 Grille
				\draw [very thin,gray,dashed] (1,0) grid (\thejourcourant+1,\thelignejour);% La grille en fin
				\draw (1,0) rectangle (\thejourcourant+1,\thelignejour);% Le cadre le plus intérieur
				\draw [thick] (\thecolminus-.5,\thelignejour+.5) rectangle (\thejourcourant+1.5,-.5);% Le cadre le plus extérieur
				%<3 Libellés
				\setcounter{ligne}{1}%
				\nligne{Page / numéro}%
				\lignesep%
				\nligne{Migraine (\emph{intensité maximale})}%
				\nligne{Durée(En heure)}%
				\nligne{Poche à glace}%
				\lignemain\lignemain\lignemain\lignemain\lignemain\lignemain\lignemain\lignesep%
				\nligne{Heure de coucher}%
				\nligne{Heure de lever}%
				\nligne{Durée du sommeil}%
				\nligne{Qualité du sommeil (\emph{de -3 à 3})}%
				\lignesep%
				\nligne{Activité physique}%
				\nligne{Hydratation}%
				\nligne{Café}%
				\nligne{Repas (\emph{de -5 à 5})}%
				\nligne{Alcool}%
				\nligne{Facteur de stress}%
				\nligne{Chiens / chats}%
				\nligne{Veille}%
				\lignemain\lignemain\lignemain%
			\end{tikzpicture}%
			\vspace*{\stretch{1}}%
		\end{landscape}%
		\addtocounter{moiscourant}{1}%
	}%
}


%<1 Pages de migraines
% créé des tableaux pour le suivit de chacune des migraines
%<2 Commandes
\newcommand\remarques[2]{%
	\textit{#1}%
	\begin{itemize}[leftmargin=*,label=\ding{113}\hspace{-3.4pt}]%
		\raggedright%
		#2%
	\end{itemize}%
}
\newcommand\itemblanc[1][autres:]{\item \textit{#1}\dotfill}%
\newcounter{cdr}% je ne peux pas utiliser directement la variables k...
\newcommand\migraines[1]{%
	\setlength{\parindent}{0pt}%
	#1) Le: \makebox[20em]{\dotfill}\par\vspace{-2ex}%
	\begin{center}%
	\begin{tikzpicture}[yscale=.4,xscale=.48]%<2 Tableau
		%<3 Grille
		\foreach \k in {0,4,...,32}{% Les colonnes grise sur le fond
			\setcounter{cdr}{\k}%
			\fill [color=gray!40]%
			(\thecdr,0) -- (\thecdr+2,0) -- (\thecdr+2,10) -- (\thecdr,10) -- cycle;%
		}%
		\draw [very thin,gray] (0,0) grid (36,10);% La grille en fin
		\draw [thick] (-1.6,11.7) rectangle (36.8,-5.6);% Le cadre le plus extérieur
		\foreach \k in {12,24}{\draw [thick] (\k,11.7) -- (\k,-5.6);}% Les deux traits verticaux sombres
		%<3 Jours
		\draw ( 0,10.3) node [above right,scale=1] {1\ier jour};%
		\draw (12,10.3) node [above right,scale=1] {2\ieme jour};%
		\draw (24,10.3) node [above right,scale=1] {3\ieme jour};%
		%<3 Heures
		\draw ( 0,10) node [above right,scale=0.6] {0};%
		\draw ( 1,10) node [above,scale=0.6] {2};%
		\draw ( 2,10) node [above,scale=0.6] {4};%
		\draw ( 3,10) node [above,scale=0.6] {6};%
		\draw ( 4,10) node [above,scale=0.6] {8};%
		\draw ( 5,10) node [above,scale=0.6] {10};%
		\draw ( 6,10) node [above,scale=0.6] {12};%
		\draw ( 7,10) node [above,scale=0.6] {14};%
		\draw ( 8,10) node [above,scale=0.6] {16};%
		\draw ( 9,10) node [above,scale=0.6] {18};%
		\draw (10,10) node [above,scale=0.6] {20};%
		\draw (11,10) node [above,scale=0.6] {22h};%
		\draw (12,10) node [above right,scale=0.6] {0};%
		\draw (13,10) node [above,scale=0.6] {2};%
		\draw (14,10) node [above,scale=0.6] {4};%
		\draw (15,10) node [above,scale=0.6] {6};%
		\draw (16,10) node [above,scale=0.6] {8};%
		\draw (17,10) node [above,scale=0.6] {10};%
		\draw (18,10) node [above,scale=0.6] {12};%
		\draw (19,10) node [above,scale=0.6] {14};%
		\draw (20,10) node [above,scale=0.6] {16};%
		\draw (21,10) node [above,scale=0.6] {18};%
		\draw (22,10) node [above,scale=0.6] {20};%
		\draw (23,10) node [above,scale=0.6] {22h};%
		\draw (24,10) node [above right,scale=0.6] {0};%
		\draw (25,10) node [above,scale=0.6] {2};%
		\draw (26,10) node [above,scale=0.6] {4};%
		\draw (27,10) node [above,scale=0.6] {6};%
		\draw (28,10) node [above,scale=0.6] {8};%
		\draw (29,10) node [above,scale=0.6] {10};%
		\draw (30,10) node [above,scale=0.6] {12};%
		\draw (31,10) node [above,scale=0.6] {14};%
		\draw (32,10) node [above,scale=0.6] {16};%
		\draw (33,10) node [above,scale=0.6] {18};%
		\draw (34,10) node [above,scale=0.6] {20};%
		\draw (35,10) node [above,scale=0.6] {22h};%
		%<3 Intensité
		\draw (-1.1,5) node [rotate=90,scale=.9] {I n t e n s i t é};%
		\draw [double,very thin] (0,5) -- (36,5);%
		\foreach \k in {1,3,...,9}{%
			\draw ( 0, \k) node [left,scale=0.6]  {\k};%
			\draw (36, \k) node [right,scale=0.6] {\k};%
		}
		%<3 Médication
		\foreach \k in {1,13,25}{%
			\draw (\k,-1) node [above right,scale=0.6] {Médicament};%
			\draw (\k+7,-1) node [above right,scale=0.6] {Dose};%
			\draw [thick] (\k, -1) rectangle (\k+10, -5);%
			\draw (\k+7,-1) -- (\k+7,-5);%
			\draw (\k,-2) -- (\k+10,-2);%
			\draw (\k,-3) -- (\k+10,-3);%
			\draw (\k,-4) -- (\k+10,-4);%
			\draw (\k,-2) node [above left,scale=0.6]{1};%
			\draw (\k,-3) node [above left,scale=0.6]{2};%
			\draw (\k,-4) node [above left,scale=0.6]{3};%
			\draw (\k,-5) node [above left,scale=0.6]{4};%
		}%
	\end{tikzpicture}%
	\end{center}%
	%<2 Colonnes de textes
	\vspace{-20pt}%
	\setlength{\parindent}{0pt}%
	\setlength{\columnsep}{5pt}%
	\setlength{\columnseprule}{.1pt}%
	\begin{multicols}{4}%
		\scriptsize%
		\begin{tikzpicture}%<3
			\node[anchor=north west,inner sep=0] at (1.4,0)%
				{\includegraphics[width=.65\linewidth,keepaspectratio=true]{tete}};%
			\node[anchor=north west,inner sep=0] at (0,-.2)%
				{{\large\bfseries Remarques}};%
			\node[anchor=north west,inner sep=0] at (0,-.7)%
				{\parbox{10em}{\textit{\footnotesize Cochez \& soulignez\\ ce qui convient}}};%
		\end{tikzpicture}%
		\par\columnbreak%
		\remarques{Types \& localisation de la douleur}{%<3
			\item pulsative/frappante/aiguille%
			\item oppressante/lourdeur/tiraillement%
			\itemblanc[]%
			\itemblanc[]%
		}%
		\remarques{Aura (troubles avant/pendant)}{%<3
			\item troubles du langage%
			\item \scalebox{.92}[1]{visuels\,(éblouissement,\,éclairs,\,taches, } champ réduit, vision double)%
			\item engourdissement (fourmillement: lèvres, mains, pieds)%
			\item faiblesse dans les bras/jambes%
			\item vertiges%
		\par\columnbreak%
			\itemblanc[]%
			\itemblanc[]%
		}%
		\remarques{Signes d'accompagnement}{%<3
			\item nausées / vomissements%
			\item Paralysie: \dotfill%
			\item sensibilité à la lumière%
			\item sensibilité au bruit%
			\itemblanc[]%
			\itemblanc[]%
		}%
		\remarques{Facteurs déclanchants}{%<3
			\item menstruation%
			\item stress: \dotfill%
			\item présence de chiens/chats%
			\item fatigue/veille%
			\item alcool: \dotfill%
			\item repas (gros / petit / très petit)%
			\item déshydratation: (Qté 24h) \dotfill%
			\itemblanc[]%
			\itemblanc[]%
		}%
		\remarques{Limitations fonctionnelles}{%<3
			\item les soins aux enfants%
			\item les loisirs%
			%\item les relations avec le partenaire%
			\item interactions (sociales / famille)%
			\item travail / école / société%
			\itemblanc[]%
			\itemblanc[]%
			\itemblanc[après la crise:]%
			\itemblanc[]%
		}%
	\end{multicols}%
}%
%<2 Combinaisons de deux par page
\newcommand\pagemigraines[1][\numexpr ((\theparmois * \theduree ) / 2)]{%
	\rohead[]{Journal des migraines}
	\addcontentsline{toc}{part}{Journal des migraines}%
	\multido{\i=1+1}{#1}{%
		\clearpage%
		\migraines{\thepage{}a}%
		\vspace*{\stretch{1}}%
		\hrule%
		\vspace*{\stretch{5}}%
		\migraines{\thepage{}b}%
	}%
}%


%<1 Colophon
\newcommand\colophon{%
	\newgeometry{hmargin=4cm}%
	\addcontentsline{toc}{part}{Colophon}%
	\newcommand\colophonin[2]{\par\texttt{##1}\\ ##2\medskip{}}%
	\thispagestyle{empty}%
	\begin{center}%
		\vspace*{\stretch{2}}%
		\small%
		Ce document a été produit avec les outils suivant:%
		\begin{multicols}{2}%
			\par\LaTeXe{}\\ composition de document\medskip%
			\colophonin{TikZ}{TikZ ist kein Zeichenprogramm,\\%
				mais produit d'exellent graphiques vectoriels}%
			\colophonin{MetaFont}{fondeur de police et chiffres elzéviriens}%
			\colophonin{KOMA-Script}{mise en page et titraille}%
			\colophonin{LaTeXmk}{gestionnaire de compilation}%
			\par\columnbreak%
			\colophonin{Vim}{éditeur de texte}%
			\colophonin{GIT}{gestions du code source}%
			\colophonin{zsh}{Shell à la complétion magique}%
			\colophonin{\url{https://framagit.org}}{Collaboration sur le code source}%
			\colophonin{Openbox}{environnement graphique}%
			\colophonin{Archlinux}{OS permettant de gérer le tout}%
		\end{multicols}%
		\bigskip%
		Toute notre gratitude aux libristes ayant choisi de mettre leurs codes à disposition,\\%
		eux qui ont compris que le seul moyen de faire circuler le savoir est de permettre,\\%
		à tout un chacun, d'avoir un accès libre (free) à la connaissance.\par\bigskip%
		Il est possible d'annoncer des corrections et de proposer des modifications ici:\\%
		\url{https://framagit.org/egide.latex/migraine/issues}\\\medskip%
		Le code source ce trouve ici:\\%
		\url{https://framagit.org/egide.latex/migraine}\\\medskip%
		Il est possible de télécharger les documents ici:\\%
		\url{https://framadrive.org/s/xzGsaGPTA3R4PwT}\\%
		ou ici:\\%
		\url{https://rnv.ch/~migraine/}%
		\vspace{\stretch{1}}%
	\end{center}%
	\restoregeometry%
}

%<1 vim: set ts=3 sts=3 sw=3 tw=100 fdm=marker foldmarker=%<,%> filetype=tex spell:%

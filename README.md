Journal des Céphalées
=====================

*date de création* : 2017/05/01

Test pour la construction d'un journal des céphalées

Le projet est créé le jeudi 27 juillet 2017.

L'idée est d'avoir un suivi des migraines et un outil pour déterminer
les facteurs déclenchants.


Destinataires du projet
-----------------------
Ce projet est personnel. Il ne prétend pas être diffusé largement.


Ce que le projet fait
---------------------
Un journal des migraines à imprimer.


Prérequis
---------
Ce n'est pas un programme, mais un fichier LaTeX. Il suffit donc d'avoir installé LaTeX.


Documentation
-------------

1. [CHANGELOG](CHANGELOG.md) Changements importants d'une version à l'autre
2. [CONTRIBUTING](CONTRIBUTING.md) indications utiles pour les personnes qui voudraient contribuer au projet
3. [LICENSE](LICENSE) termes de la licence
4. [Dépôt Git](https://framagit.org/egide.latex/migraine)
